'use strict';

angular
  .module('projectJuneTerm')
  .controller('DashboardController', ['$scope', '$log', '$rootScope', 'refservice', function ($scope, $log, $rootScope, refservice) {

    var sentMails = function () {
      refservice.email_count().then(function (data) {
          $scope.sentEmails = data.data.email_count;
        }, function (err) {
          $log.debug(err);
        }
      );
    }

    var joinedCount = function () {
      refservice.getJoinedCount().then(function (data) {
        $scope.joinedCount = data.data.getJoinedCount;
      }, function (err) {
        $log.debug(err);
      });
    }
    joinedCount();
    sentMails();

  }]);
