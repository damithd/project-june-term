'use strict';

angular
  .module('projectJuneTerm')
  .controller('ReferController', ['$scope', '$log', '$rootScope', 'refservice', function ($scope, $log, $rootScope, refservice) {
    $scope.user = {
      email_ref: "",
      message: "I am Learning with EduLink.Join ME!!!"

    }
    $scope.send_email = function () {
      refservice.send_email($scope.user.email_ref, $scope.user.message);

    }

  }]);
