'use strict';

angular
  .module('projectJuneTerm')
  .controller('AdminDashboardController', ['$scope', '$log', '$rootScope', 'refservice', function ($scope, $log, $rootScope, refservice) {
    $scope.user = {
      id: ''
    }
    $scope.sendUid = function () {
      refservice.joinedCount($scope.user.id);
    }
  }]);
