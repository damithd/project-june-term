(function() {
  'use strict';

  angular
    .module('projectJuneTerm')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
