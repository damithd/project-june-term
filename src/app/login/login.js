
'use strict'

angular
  .module('projectJuneTerm')

  .controller('LoginController', ['$scope','$log','$rootScope','$auth','$state',function($scope,$log,$rootScope,$auth,$state){
    $scope.name='';
    $scope.password='';
    $scope.autho=function() {
      $auth.login({
        username: $scope.name,
        password: $scope.password
      })
        .then(function (response) {

          $log.debug(response)
          $state.go('home');
        })
        .catch(function (response) {
          $log.debug(response)

        });

    }
$scope.signUp=function(){
  $state.go('signup');

}
  }]);
