(function() {
  'use strict';

  angular
    .module('projectJuneTerm', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ui.router', 'ngMaterial','satellizer']);

})();
