(function() {
  'use strict';

  angular
    .module('projectJuneTerm')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastr,$mdThemingProvider,$authProvider) {
    $authProvider.loginUrl='/auth/login'
    $authProvider.signupUrl='/auth/signup'
    // Enable log
    $logProvider.debugEnabled(true);
    $mdThemingProvider.theme('docs-dark', 'default')
      .primaryPalette('yellow')
      .dark();
    // Set options third-party lib
    toastr.options.timeOut = 3000;
    toastr.options.positionClass = 'toast-top-right';
    toastr.options.preventDuplicates = true;
    toastr.options.progressBar = true;
  }

})();
