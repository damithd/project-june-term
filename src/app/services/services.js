(function () {
  'use strict';

  angular.module('projectJuneTerm')
    .service(
    "refservice", ['$http', '$q', '$log', '$auth',
      function ($http, $q, $log, baseUrl, $auth) {


        var send_email = function (email, message) {
          var promise = $http({
            method: "post",
            url: 'http://localhost:3000/auth/send',
            headers: {
              "Content-Type": "application/json"
            },
            data: {
              email: email,
              message: message,

            }
          });
          promise.then(handleSuccess, handleError);
          return promise;
        };


        var email_count = function () {
          var promise = $http({
            method: "get",
            url: 'http://localhost:3000/auth/emailCount',
            headers: {
              "content-Type": "application/json"
            },
          });
          promise.then(handleSuccess, handleError);
          return promise;
        };


        var joinedCount = function (uid) {
          var promise = $http({
            method: "post",
            url: 'http://localhost:3000/auth/joinedCount',
            headers: {
              "Content-Type": "application/json"
            },
            data: {
              uid: uid
            }
          });
          promise.then(handleSuccess, handleError);
          return promise;
        };
        var getJoinedCount = function () {
          var promise = $http({
            method: "get",
            url: 'http://localhost:3000/auth/getJoinedCount',
            headers: {
              "content-Type": "application/json"
            },
          });
          promise.then(handleSuccess, handleError);
          return promise;
        };

        var handleSuccess = function (data) {
          $log.debug("[INFO] : Received Data from Server");
          $log.debug(data);
        };
        var handleError = function (data) {
          $log.debug("[ERROR] : Data Receiving Error!!");
          $log.debug(data);
        };

        return ({
          send_email: send_email,
          email_count: email_count,
          joinedCount:joinedCount,
          getJoinedCount:getJoinedCount
        });

      }]
  );
})();
