
'use strict'

angular
  .module('projectJuneTerm')

  .controller('SignupController', ['$scope','$log','$rootScope','$auth','$state',function($scope,$log,$rootScope,$auth,$state){
    $scope.name='';
    $scope.password='';
    $scope.autho=function() {
      $auth.signup({
        username: $scope.name,
        password: $scope.password
      })
        .then(function (response) {

          $log.debug(response)
          $state.go('home');
        })
        .catch(function (response) {
          $log.debug(response)

        });

    }

  }]);
