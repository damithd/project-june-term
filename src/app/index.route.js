(function () {
  'use strict';

  angular
    .module('projectJuneTerm')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/signup/signup.html',
        controller: 'SignupController',
        controllerAs: 'signup'
      })


      .state('home.refer', {
        url: 'refer',
        views: {
          'main': {
            templateUrl: 'app/components/refer/refer.html',
            controller: 'ReferController',
            controllerAs: 'refer'
          }
        }
      })
      .state('home.dashboard', {
        url: 'dashboard',
        views: {
          'main': {
            templateUrl: 'app/components/dashboard/dashboard.html',
            controller: 'DashboardController',
            controllerAs: 'dashboard'
          }
        }

      })

      .state('home.adminDashboard', {
        url: 'adminDashboard',
        views: {
          'main': {
            templateUrl: 'app/components/adminDashboard/adminDashboard.html',
            controller: 'AdminDashboardController',
            controllerAs: 'adminDashboard'
          }
        }

      })
    $urlRouterProvider.otherwise('/');
  }

})();
