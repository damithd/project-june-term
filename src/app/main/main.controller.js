(function () {
  'use strict';

  angular
    .module('projectJuneTerm')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, $mdSidenav, $log, $scope, $mdUtil, $rootScope, $state, $auth) {
/*    var init = function () {
      if (!$auth.isAuthenticated()) {
        $state.go('login');
      }
      else $state.go('home.dashboard');
    }
    init();*/

    $scope.signup=function(){
      $state.go('signup');

    }
    $scope.logout = function () {
      $auth.logout();
      localStorage.clear();
      $state.go('login');
    }
    $scope.dashboard = function () {
      $state.go('home.dashboard');
    }
    $scope.home = function () {
      $state.go('home');
    }

    $scope.refer = function () {
      $state.go('home.refer');
    }
    $scope.adminDashboard = function () {
      $state.go('home.adminDashboard');
    }

    $scope.role=$auth.getPayload().role;



    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildToggler(navID) {
      var debounceFn =  $mdUtil.debounce(function(){
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      },300);
      return debounceFn;
    }
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });
    };
    $scope.close = function () {
      $mdSidenav('right').close()
        .then(function () {
          $log.debug("close RIGHT is done");
        });
    };





  }

})();

